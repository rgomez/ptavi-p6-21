#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys


try:
    SERVER = 'localhost'
    METHOD = sys.argv[1]
    RECEIVER = sys.argv[2]
    RECEIVERNAME = RECEIVER.split("@")[0]
    RECEIVERIP = str(RECEIVER.split("@")[1])
    SIPPORT = int(RECEIVER.split(":")[1])
    SDPDescription = f"\r\nv=0\r\no=raulgomez@urjc.es 127.0.0.1\r\nt=0\r\nm=audio {SIPPORT} RTP"
    cabecera1 = "Content - Type: application / sdp\r\n"
    length = len(SDPDescription)
    cabecera2 = f"Content-Length: {length}\r\n"
    SDPDescription = cabecera1 + cabecera2 + SDPDescription
except IndexError:
    print("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")




def main():
    # Creamos el socket y lo configuramos
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, SIPPORT))
            REQUEST = METHOD.upper() + " " + "sip:" + RECEIVERNAME + '@' + RECEIVERIP + " SIP/2.0" + "\r\n"
            if METHOD == "INVITE":
                REQUEST += SDPDescription
            print(REQUEST)
            my_socket.send(REQUEST.encode('utf-8') + b'\r\n')
            data = my_socket.recv(2048)
            response = data.decode('utf-8')
            response2 = response.split("\r\n")
            if METHOD == "INVITE" and response2 == "SIP/2.0 200 OK":
                REQUEST = f"ACK sip: {RECEIVER} SIP/2.0"
                print(REQUEST)
                my_socket.send(REQUEST.encode('utf-8') + b'\r\n')
            print("Recibido: ")
            print(data.decode('utf-8'))
            print("Terminando programa...")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
