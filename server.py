#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""
import random
import socketserver
import sys
import simplertp


try:
    IP = str(sys.argv[1])
    SIPPORT = int(sys.argv[2])
    AUDIO_FILE = sys.argv[3]
except IndexError:
    print("Usage: python3 server.py <IP> <port> <audio_file>")


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    diccionario = {}

    def handle(self):
        readline = self.rfile.read()
        method = readline.decode('utf-8').split(" ")[0]
        ipclient = self.client_address[0]
        portclient = self.client_address[1]
        user = readline.decode('utf-8').split(":")[1]
        SDPDescription = f"v=0\r\no={user}\r\nt=0\r\nm=audio {SIPPORT} RTP"
        self.diccionario["usuario"] = [user]
        print(self.diccionario)
        if method != "INVITE" and method != "ACK" and method != "BYE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
        elif method == "INVITE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n" + SDPDescription.encode('utf-8'))
        elif method == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        elif method == "ACK":
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=random.randrange(10000))
            audio = simplertp.RtpPayloadMp3(AUDIO_FILE)
            simplertp.send_rtp_packet(RTP_header, audio, ipclient, portclient)

        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


def main():
    # Creamos servidor de eco y escuchamos
    with socketserver.UDPServer(('', SIPPORT), EchoHandler) as serv:
        print("Listening...")
        serv.serve_forever()


if __name__ == "__main__":
    main()
